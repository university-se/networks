package it.colbys.signal;

public class Context {

    private boolean isCompleted = false;
    private boolean rsync = false;
    private int line = 0;

    public synchronized boolean isCompleted() {
        return isCompleted;
    }

    public synchronized void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public synchronized boolean isRsync() {
        return rsync;
    }

    public synchronized void setRsync(boolean rsync) {
        this.rsync = rsync;
    }

    public synchronized int getLine() {
        return line;
    }

    public synchronized void setLine(int line) {
        this.line = line;
    }
}
