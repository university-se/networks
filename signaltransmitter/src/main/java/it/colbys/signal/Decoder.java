package it.colbys.signal;

import java.util.Random;

public class Decoder extends Thread {

    private Context context;
    private Random random;

    public Decoder(final Context context) {
        this.context = context;
        random = new Random();
    }

    @Override
    public void run() {
        try {
            while (!context.isRsync() && !context.isCompleted()) {
                if (checkForResync()) break;
                if (context.getLine() == 2) {
                    System.out.println("Got bit 1");
                }
                if (context.getLine() == 1) {
                    System.out.println("Got bit 0");
                }
                if (checkForResync()) break;
                if (checkForResync()) break;
                if (checkForResync()) break;
                if (checkForResync()) break;
                if (checkForResync()) break;
                if (checkForResync()) break;
                if (checkForResync()) break;
                if (checkForResync()) break;

                Thread.sleep(random.nextInt(10) % 9);
            }
            System.out.println("Decoder rebooted\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkForResync() throws InterruptedException {
        Thread.sleep(10);
        return context.isRsync();
    }
}
