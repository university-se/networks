package it.colbys.signal;

public class Application {

    public static void main(String[] args) throws Exception {
        Context context = new Context();

        Listener listener = new Listener(context);
        Coder coder = new Coder(context, new int[]{ 1, 0, 0, 1, 0, 0, 0, 1, 1, 1 });
        listener.start();
        System.out.println("Starting to listen...");
        Thread.sleep(315);
        coder.start();
        System.out.println("Transmitting message...\n");
        Thread.sleep(6000);

        System.out.println("Message transmitted, terminating listener...");
        context.setCompleted(true);
    }
}
