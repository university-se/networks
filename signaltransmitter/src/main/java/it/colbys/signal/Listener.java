package it.colbys.signal;

public class Listener extends Thread {

    private Context context;

    public Listener(final Context context) {
        this.context = context;
    }

    @Override
    public void run() {
        try {
            while (!context.isCompleted()) {
                int buffer = context.getLine();
                Thread.sleep(20);
                if (Math.abs(buffer - context.getLine()) > 0) {
                    System.out.println("Synced");
                    context.setRsync(true);
                    Thread.sleep(10);
                    context.setRsync(false);
                    Decoder decoder = new Decoder(context);
                    decoder.start();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
