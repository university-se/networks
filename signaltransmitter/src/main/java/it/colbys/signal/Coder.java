package it.colbys.signal;

public class Coder extends Thread {

    private Context context;
    private int[] message;

    public Coder(final Context context, final int[] message) {
        this.context = context;
        this.message = message;
    }

    @Override
    public void run() {
        try {
            for (int value : message) {
                System.out.println(String.format("Coded bit %d", value));
                context.setLine(value + 1);
                Thread.sleep(100);
            }
            context.setLine(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
