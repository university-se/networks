package it.colbys.transmitter.utils;

public class Logger {

    public static void info(final String text, final Object... args) {
        log("INFO", text, args);
    }

    public static void warning(final String text, final Object... args) {
        log("WARNING", text, args);
    }

    public static void error(final String text, final Object... args) {
        log("ERROR", text, args);
    }

    private static void log(final String level, final String text, final Object... args) {
        System.out.println("[" + level + "] " + String.format(text, args));
    }
}
