package it.colbys.transmitter.utils;

import it.colbys.transmitter.core.Message;

import java.util.HashMap;
import java.util.Map;

public class MessageParser {

    private static final int MESSAGE_LENGTH = 4;
    private static Map<Character, Integer> allowedSymbols;

    static {
        allowedSymbols = new HashMap<>();
        allowedSymbols.put('0', 0);
        allowedSymbols.put('1', 1);
    }

    public static int[] parse(final String rawMessage) {
        int[] message = new int[4];
        try {
            for (int i = 0; i < MESSAGE_LENGTH; i++) {
                char currentChar = rawMessage.charAt(i);
                Integer byteValue = allowedSymbols.get(currentChar);
                if (byteValue == null) {
                    Logger.warning("Unknown character: %c. Only 0 and 1 can be accepted.", currentChar);
                    message[i] = 2;
                    continue;
                }
                message[i] = byteValue;
            }
        } catch (StringIndexOutOfBoundsException e) {
            Logger.error("Invalid message was provided. Message should be 4 bits long and contain only 0 and 1.");
            System.exit(1);
        }
        return message;
    }

    public static String messageToString(final Message message) {
        StringBuilder builder = new StringBuilder();
        int[] data = message.getMessage();
        int length = message.getLength();

        for (int i = 0; i < length; i++) {
            builder.append(data[i]);
        }

        return builder.toString();
    }
}
