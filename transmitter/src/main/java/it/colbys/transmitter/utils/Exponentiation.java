package it.colbys.transmitter.utils;

public class Exponentiation {

    private final int base;

    private int power;

    public Exponentiation(final int base) {
        this.base = base;
        power = 0;
    }

    public int getExponent() {
        return (int) Math.pow(base, power++);
    }

    public void reset() {
        power = 0;
    }
}
