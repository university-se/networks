package it.colbys.transmitter;

import it.colbys.transmitter.coders.Coder;
import it.colbys.transmitter.coders.CoderStorage;
import it.colbys.transmitter.commandline.ArgumentsParser;
import it.colbys.transmitter.commandline.OptionsBuilder;
import it.colbys.transmitter.core.Message;
import it.colbys.transmitter.core.MessageValidator;
import it.colbys.transmitter.core.Transmitter;
import it.colbys.transmitter.utils.Logger;
import it.colbys.transmitter.utils.MessageParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

public class Application {

    public static void main(String[] args) {
        Options options = OptionsBuilder.buildOptions();
        CommandLine cmd = ArgumentsParser.parseArguments(options, args);

        String rawMessage = cmd.getOptionValue("message");
        String methodName = cmd.getOptionValue("method");
        String rawBitsToBreak = cmd.getOptionValue("bits-to-break");

        Coder coder = CoderStorage.getCoder(methodName);

        int[] message = MessageParser.parse(rawMessage);
        if (!MessageValidator.validate(message)) {
            Logger.error("Invalid message was provided. Message should be 4 bits long and contain only 0 and 1.");
            System.exit(1);
        }

        Message encodedMessage = coder.encode(message);

        Logger.info("Attempting to send message %s...", MessageParser.messageToString(encodedMessage));
        Logger.info("Transmitting...");

        Message receivedMessage = Transmitter.transmit(encodedMessage, Integer.parseInt(rawBitsToBreak));
        Logger.info(
                "Received message %s. Attempting to validate it...",
                MessageParser.messageToString(receivedMessage)
        );

        if (!coder.validate(encodedMessage)) {
            Logger.error("Message was broken during transmission.");
            System.exit(1);
        }

        Logger.info("Message was received correctly.");
    }
}
