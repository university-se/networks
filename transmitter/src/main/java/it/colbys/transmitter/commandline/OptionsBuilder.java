package it.colbys.transmitter.commandline;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class OptionsBuilder {

    public static Options buildOptions() {
        Option message = new Option("msg", "message", true, "message to transmit, may contain only 0 and 1 with total length of 4");
        Option method = new Option("m", "method", true, "method of message transmission");
        Option bitsToBreak = new Option("b", "bits-to-break", true, "how many bits should be broken during transmission");

        message.setRequired(true);
        method.setRequired(true);
        bitsToBreak.setRequired(true);

        return new Options().addOption(message).addOption(method).addOption(bitsToBreak);
    }
}
