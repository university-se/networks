package it.colbys.transmitter.coders;

import it.colbys.transmitter.utils.Logger;

import java.util.HashMap;
import java.util.Map;

public class CoderStorage {

    private static Map<String, Coder> storage;

    static {
        storage = new HashMap<>();
        storage.put("parity", new ParityCoder());
        storage.put("berger", new BergerCoder());
        storage.put("hamming", new HammingCoder());
    }

    public static Coder getCoder(final String coderName) {
        Coder coder = storage.get(coderName);
        if (coder == null) {
            Logger.error("Unknown error-detection method, terminating program.");
            System.exit(1);
        }
        return coder;
    }
}
