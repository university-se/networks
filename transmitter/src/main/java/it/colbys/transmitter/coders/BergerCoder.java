package it.colbys.transmitter.coders;

import it.colbys.transmitter.core.Message;
import it.colbys.transmitter.core.MessageBuilder;

public class BergerCoder implements Coder {

    private static final int INFO_LENGTH = 4;
    private static final int MESSAGE_LENGTH = 7;
    private static final int CHECKSUM_LENGTH = 3;

    @Override
    public Message encode(int[] message) {
        int oneCounter = 0;
        for (int i = 0; i < INFO_LENGTH; i++) {
            if (message[i] == 1) {
                oneCounter++;
            }
        }

        int[] result = new int[MESSAGE_LENGTH];
        int[] binaryCounter = convertDecimalToBinary(oneCounter, 3);
        reverseBinary(binaryCounter);
        System.arraycopy(message, 0, result, 0, INFO_LENGTH);
        System.arraycopy(binaryCounter, 0, result, 4, CHECKSUM_LENGTH);

        return new MessageBuilder().changeLength(MESSAGE_LENGTH).changeData(result).build();
    }

    @Override
    public boolean validate(Message message) {
        int[] data = message.getMessage();

        int oneCounter = 0;
        for (int i = 0; i < INFO_LENGTH; i++) {
            if (data[i] == 1) {
                oneCounter++;
            }
        }

        int[] checksum = new int[CHECKSUM_LENGTH];
        for (int i = 0; i < CHECKSUM_LENGTH; i++) {
            checksum[i] = data[i + 4];
        }

        int [] binaryCounter = convertDecimalToBinary(oneCounter, CHECKSUM_LENGTH);
        reverseBinary(binaryCounter);

        return compareBinary(binaryCounter, checksum, CHECKSUM_LENGTH);
    }

    private int[] convertDecimalToBinary(final int decimal, int size) {
        int[] binary = new int[size];
        int count = 0;
        for (int c = 2; c >= 0; c--) {
            int k = decimal >> c;

            if ((k & 1) == 1) {
                binary[count] = 1;
            } else {
                binary[count] = 0;
            }
            count++;
        }
        return binary;
    }

    private boolean compareBinary(int[] first, int[] second, int size) {
        boolean result = true;
        for (int i = 0; i < size; i++) {
            if (first[i] != second[i]) {
                result = false;
                break;
            }
        }
        return result;
    }

    private void reverseBinary(int[] binary) {
        for (int i = 0; i < binary.length; i++) {
            binary[i] = reverseBit(binary[i]);
        }
    }

    private static int reverseBit(int bit) {
        return bit == 1 ? 0 : 1;
    }
}
