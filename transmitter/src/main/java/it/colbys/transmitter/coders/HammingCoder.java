package it.colbys.transmitter.coders;

import it.colbys.transmitter.core.Message;
import it.colbys.transmitter.core.MessageBuilder;
import it.colbys.transmitter.utils.Exponentiation;
import it.colbys.transmitter.utils.Logger;
import it.colbys.transmitter.utils.MessageParser;

import java.util.ArrayList;
import java.util.List;

public class HammingCoder implements Coder {

    private static final int MESSAGE_LENGTH = 8;
    private static final int CHECKSUM_COUNT = 3;

    @Override
    public Message encode(int[] message) {
        Exponentiation exp = new Exponentiation(2);
        int[] encodedMessage = message;

        for (int i = 0; i < CHECKSUM_COUNT; i++) {
            int rawChecksumPos = exp.getExponent();
            encodedMessage = expandArray(encodedMessage);
            shiftArray(encodedMessage, rawChecksumPos - 1);
        }
        exp.reset();

        calculateChecksum(encodedMessage);

        int checksum = 0;
        for (int i = 0; i < MESSAGE_LENGTH - 1; i++) {
            checksum ^= encodedMessage[i];
        }

        encodedMessage = expandArray(encodedMessage);
        encodedMessage[7] = checksum;

        return new MessageBuilder().changeData(encodedMessage).changeLength(MESSAGE_LENGTH).build();
    }

    @Override
    public boolean validate(Message message) {
        int[] data = message.getMessage();

        int c1 = data[0] ^ data[2] ^ data[4] ^ data[6];
        int c2 = data[1] ^ data[2] ^ data[5] ^ data[6];
        int c3 = data[3] ^ data[4] ^ data[5] ^ data[6];

        int checksum = 0;

        for (int i = 0; i < MESSAGE_LENGTH - 1; i++) {
            checksum ^= data[i];
        }

        boolean validChecksum = data[7] == checksum;
        boolean invalidMessage = c1 != 0 || c2 != 0 || c3 != 0;

        if (validChecksum && invalidMessage) {
            Logger.error("Encountered two errors in the message, unable to fix them.");
            return false;
        }
        if (!validChecksum && invalidMessage) {
            Logger.warning("Encountered error within received message. Attempting to repair it...");
            List<Integer> positions = new ArrayList<>();
            int[] backup = data.clone();
            calculateChecksum(backup);

            for (int i = 0; i < message.getLength(); i++) {
                if (data[i] != backup[i]) {
                    positions.add(i + 1);
                }
            }

            int position = positions.stream().reduce(0, Integer::sum) - 1;
            data[position] = reverseBit(data[position]);

            Message msg = new Message(data, MESSAGE_LENGTH);

            Logger.info("Successfully restored message %s", MessageParser.messageToString(msg));
        }

        return true;
    }

    private int[] expandArray(final int[] array) {
        int[] newArray = new int[array.length + 1];
        System.arraycopy(array, 0, newArray, 0, array.length);
        return newArray;
    }

    private void shiftArray(final int[] array, final int shiftPos) {
        if (array.length - shiftPos >= 0) {
            System.arraycopy(array, shiftPos, array, shiftPos + 1, array.length - shiftPos - 1);
        }
        array[shiftPos] = 0;
    }

    private void calculateChecksum(final int[] array) {
        array[0] = array[2] ^ array[4] ^ array[6];
        array[1] = array[2] ^ array[5] ^ array[6];
        array[3] = array[4] ^ array[5] ^ array[6];
    }

    private static int reverseBit(int bit) {
        return bit == 1 ? 0 : 1;
    }
}
