package it.colbys.transmitter.coders;

import it.colbys.transmitter.core.Message;

import java.util.Arrays;

public class ParityCoder implements Coder {

    private static final int MESSAGE_LENGTH = 4;

    @Override
    public Message encode(final int[] message) {
        int newMessageLength = 5;
        int[] tempMessage = Arrays.copyOf(message, newMessageLength);

        int checksum = 0;
        for (int i = 0; i < MESSAGE_LENGTH; i++) {
            checksum ^= tempMessage[i];
        }
        tempMessage[4] = checksum;

        return new Message(tempMessage, newMessageLength);
    }

    @Override
    public boolean validate(final Message message) {
        int[] data = message.getMessage();
        int checksum = 0;

        for (int i = 0; i < MESSAGE_LENGTH; i++) {
            checksum ^= data[i];
        }

        return data[4] == checksum;
    }
}
