package it.colbys.transmitter.coders;

import it.colbys.transmitter.core.Message;

public interface Coder {

    Message encode(int[] message);

    boolean validate(Message message);
}
