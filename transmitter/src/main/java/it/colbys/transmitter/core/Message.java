package it.colbys.transmitter.core;

import java.util.Arrays;
import java.util.Objects;

public class Message {

    private final int[] message;
    private final int length;

    public Message(final int[] message, final int length) {
        this.message = message;
        this.length = length;
    }

    public int[] getMessage() {
        return message;
    }

    public int getLength() {
        return length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return length == message1.length &&
                Arrays.equals(message, message1.message);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(length);
        result = 31 * result + Arrays.hashCode(message);
        return result;
    }

    @Override
    public String toString() {
        return "message=" + Arrays.toString(message) +
                ", length=" + length;
    }
}
