package it.colbys.transmitter.core;

import java.util.Arrays;
import java.util.Random;

public class Transmitter {

    public static Message transmit(final Message message, int bitsToBreak) {
        if (bitsToBreak <= 0) {
            return message;
        }

        Random random = new Random();
        int[] data = message.getMessage();
        int length = message.getLength();
        MessageBuilder alteredMessage = new MessageBuilder().changeLength(length);

        int[] changedBitIndexes = new int[length];
        Arrays.fill(changedBitIndexes, -1);

        do {
            int bitToChange = random.nextInt(length);
            if (!checkIfChanged(changedBitIndexes, bitToChange)) {
                int changedBit = data[bitToChange];
                data[bitToChange] = reverseBit(changedBit);
                changedBitIndexes[bitToChange] = bitToChange;
                alteredMessage.changeData(data);
                bitsToBreak--;
            }
        } while (bitsToBreak != 0);

        return alteredMessage.build();
    }

    private static boolean checkIfChanged(int[] indexes, int index) {
        for (int value : indexes) {
            if (value == index) {
                return true;
            }
        }
        return false;
    }

    private static int reverseBit(int bit) {
        return bit == 1 ? 0 : 1;
    }
}
