package it.colbys.transmitter.core;

import java.util.HashSet;
import java.util.Set;

public class MessageValidator {

    private static final int MESSAGE_LENGTH = 4;

    public static boolean validate(final int[] message) {
        for (int i = 0; i < MESSAGE_LENGTH; i++) {
            int currentByte = message[i];
            if (currentByte != 0 && currentByte != 1) {
                return false;
            }
        }
        return true;
    }
}
