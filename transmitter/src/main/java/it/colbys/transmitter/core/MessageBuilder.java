package it.colbys.transmitter.core;

public class MessageBuilder {

    private int[] data;
    private int length;

    public MessageBuilder changeData(int[] data) {
        this.data = data;
        return this;
    }

    public MessageBuilder changeLength(int length) {
        this.length = length;
        return this;
    }

    public Message build() {
        return new Message(data, length);
    }
}
