#include <stdio.h>
#include <stdlib.h>
#include "../core/message.h"
#include "../core/random.h"
#include "transmitter.h"

int check_if_changed(int*, int, int);

void fill_array(int*, int);

Message transmit(Message message, int bits_to_break) {
    if (bits_to_break == 0) {
        return message;
    }
    int* changed_bits_indexes = malloc(bits_to_break * sizeof(int));
    fill_array(changed_bits_indexes, message.size);

    do {
        int bit_to_change = get_random(message.size);
        if (!check_if_changed(changed_bits_indexes, bit_to_change, message.size)) {
            int changed_bit = message.data[bit_to_change];
            message.data[bit_to_change] = !changed_bit;
            changed_bits_indexes[bit_to_change] = bit_to_change;
            bits_to_break--;
        }
    } while (bits_to_break != 0);

    return message;
}

int check_if_changed(int* indexes, int index, int size) {
    for (int i = 0; i < size; i++) {
        if (indexes[i] == index) {
            return 1;
        }
    }

    return 0;
}

void fill_array(int* array, int size) {
    for (int i = 0; i < size; i++) {
        array[i] = -1;
    }
}
