#ifndef FILTER_H
#define FILTER_H

#include "../core/byte.h"

int check_message(byte_t* message);

#endif /* FILTER_H */