#include "../core/byte.h"
#include "filter.h"

int check_message(byte_t* message) {
    for (int i = 0; i < 4; i++) {
        byte_t current_byte = message[i];
        if (current_byte != 0 && current_byte != 1) {
            return 1;
        }
    }
    return 0;
}
