#include <stdlib.h>
#include <stdio.h>

#include "core/string.h"
#include "core/message.h"
#include "core/byte.h"
#include "core/hash.h"
#include "filter/filter.h"
#include "parser/parser.h"
#include "transmitter/transmitter.h"

#include "coding/parity.h"
#include "coding/berger.h"

#define PARITY_CHECK 131715819161
#define HAMMING_CHECK 7958088918753
#define BERGER_CHECK 115543143479

int main(int argc, const string_t argv[]) {
    hash_t method_hash = hash(argv[1]);
    int bits_to_break = char_to_int(argv[3][0]);
    Message (*encode)(byte_t*);
    int (*decode)(Message);
    switch (method_hash) {
        case PARITY_CHECK: {
            encode = parity__encode;
            decode = parity__decode;
            break;
        }
        case BERGER_CHECK: {
            encode = berger__encode;
            decode = berger__decode;
            break;
        }
        default: {
            printf("[ERROR] Unknown error-correction method, "
                "terminating program.\n");
            return 1;
        }
    }

    byte_t* raw_message = string_to_byte(argv[2]);
    if (check_message(raw_message)) {
        printf("[ERROR] Invalid message was provided. "
            "Message should be 4 bytes long and contain only 0 and 1.\n");
        free(raw_message);
        return 1;
    }

    Message message = encode(raw_message);

    printf("[INFO] Attempting to send message %s...\n", 
        message_to_string(message));

    printf("[INFO] Transmitting...\n");
    Message received_message = transmit(message, bits_to_break);

    printf("[INFO] Received message %s. "
        "Attempting to decode it...\n", message_to_string(message));

    if (decode(message)) {
        printf("[ERROR] Message was broken during transmittion.\n");
        free(raw_message);
        return 1;
    }

    printf("[INFO] Message was received correctly.\n");
    free(raw_message);
    return 0;
}
