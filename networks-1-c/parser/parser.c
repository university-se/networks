#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../core/byte.h"
#include "../core/string.h"
#include "../core/message.h"
#include "parser.h"

byte_t* string_to_byte(string_t string) {
    byte_t* parsed_string = malloc(4 * sizeof(byte_t));
    for (int i = 0; i < 4; i++) {
        switch (string[i]) {
            case '0': {
                parsed_string[i] = 0;
                break;
            }
            case '1': {
                parsed_string[i] = 1;
                break;
            }
            default: {
                printf("[WARNING] Unknown character: %c. Only 0 and 1 can be accepted.\n", string[i]);
                parsed_string[i] = 2;
                break;
            }
        }
    }
    return parsed_string;
}

string_t message_to_string(Message message) {
    byte_t* message_data = message.data;
    int message_size = message.size;

    string_t string = malloc(message_size * sizeof(char));
    for (int i = 0; i < message_size; i++) {
        switch (message_data[i]) {
            case 0: {
                string[i] = '0';
                break;
            }
            case 1: {
                string[i] = '1';
                break;
            }
            default: {
                printf("[WARNING] Unknown byte: %d. Only 0 and 1 are acceptable.\n", message_data[i]);
                break;
            }
        }
    }
    return string;
}

int char_to_int(char character) {
    return character - '0';
}
