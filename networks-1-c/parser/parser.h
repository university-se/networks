#ifndef parser_h
#define parser_h

#include "../core/byte.h"
#include "../core/message.h"
#include "../core/string.h"

byte_t* string_to_byte(string_t);

string_t message_to_string(Message);

int char_to_int(char);

#endif /* parser_h */
