#include "hash.h"

hash_t hash(const char *str) {
    hash_t hash = 0;
    int c;

    while (c = *str++) {
        hash = ((hash << 6) + hash) + c;
    }

    return hash;
}
