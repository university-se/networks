#ifndef MESSAGE_H
#define MESSAGE_H

#include "byte.h"

typedef struct {
    byte_t* data;
    int size;
} Message;

#endif
