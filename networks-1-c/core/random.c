#include <stdlib.h>
#include <time.h>
#include "random.h"

int get_random(int limit) {
    srand(time(NULL));
    return rand() % (limit);
}
