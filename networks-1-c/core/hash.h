#ifndef HASH_H
#define HASH_H

typedef unsigned long hash_t;

hash_t hash(const char *str);

#endif /* HASH_H */