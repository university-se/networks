#include <stdio.h>
#include <stdlib.h>

#include "../core/byte.h"
#include "parity.h"

Message parity__encode(byte_t* message) {
    byte_t* temp_message = realloc(message, 5 * sizeof(byte_t));
    if (temp_message == NULL) {
        printf("[ERROR] Unable to reallocate memory for message, terminating program.\n");
        exit(1);
    }

    byte_t checksum = 0;
    message = temp_message;
    for (int i = 0; i < 4; i++) {
        checksum ^= message[i];
    }
    message[4] = checksum;
    
    Message result;
    result.data = message;
    result.size = 5;
    return result;
}

int parity__decode(Message message) {
    byte_t* message_data = message.data;
    byte_t checksum = 0;
    for (int i = 0; i < 4; i++) {
        checksum ^= message_data[i];
    }

    if (checksum != message_data[4]) {
        return 1;
    }

    return 0;
}
