#ifndef PARITY_H
#define PARITY_H

#include "../core/byte.h"
#include "../core/message.h"

Message parity__encode(byte_t*);

int parity__decode(Message);

#endif