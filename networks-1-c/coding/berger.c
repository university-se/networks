#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../core/byte.h"
#include "berger.h"

byte_t* convert_decimal_to_binary(int decimal, int size) {
    byte_t* binary = malloc(size);
    int count = 0;
    for (int c = 2; c >= 0; c--) {
        int k = decimal >> c;
        
        if (k & 1) {
            binary[count] = 1;
        } else {
            binary[count] = 0;
        }
        count++;
    }
    return binary;
}

int compare_binary(byte_t* first, byte_t* second, int size) {
    for (int i = 0; i < size; i++) {
        if (first[i] != second[i]) {
            return 1;
        }
    }
    return 0;
}

void reverse_binary(byte_t* binary, int size) {
    for (int i = 0; i < size; i++) {
        binary[i] = !binary[i];
    }
}

Message berger__encode(byte_t* message) {
    int one_counter = 0;
    for (int i = 0; i < 4; i++) {
        if (message[i] == 1) {
            one_counter++;
        }
    }

    byte_t* result = malloc(7 * sizeof(byte_t));
    byte_t* binary_counter = convert_decimal_to_binary(one_counter, 3);
    reverse_binary(binary_counter, 3);
    memcpy(result, message, 4 * sizeof(byte_t));
    memcpy(result + 4, binary_counter, 3 * sizeof(byte_t));

    free(message);
    free(binary_counter);

    Message msg;
    msg.data = result;
    msg.size = 7;
    return msg;
}

int berger__decode(Message message) {
    byte_t* message_data = message.data;
    int one_counter = 0;
    for (int i = 0; i < 4; i++) {
        if (message_data[i] == 1) {
            one_counter++;
        }
    }
    byte_t* checksum = malloc(3 * sizeof(byte_t));
    for (int i = 0; i < 3; i++) {
        checksum[i] = message_data[i + 4];
    }

    byte_t* binary_counter = convert_decimal_to_binary(one_counter, 3);
    reverse_binary(checksum, 3);

    return compare_binary(binary_counter, checksum, 3);
    return 0;
}
