#ifndef BERGER_H
#define BERGER_H

#include "../core/byte.h"
#include "../core/message.h"

Message berger__encode(byte_t*);

int berger__decode(Message);

#endif